using UnityEngine;
using DKP.VR;

using Photon.Pun;
public class Head : MonoBehaviour
{
    [SerializeField] private Transform rootObject, followObject;
    [SerializeField] private Vector3 positionOffset, rotationOffset, headBodyOffset;

    [SerializeField] private PhotonView view;
    private void LateUpdate()
    {
        if (!view.IsMine)
            return;
        rootObject.position = transform.position + headBodyOffset;
        rootObject.forward = Vector3.ProjectOnPlane(VRPlatformSetting.instance.oVRCamera.transform.up, Vector3.up).normalized;

        transform.position = VRPlatformSetting.instance.oVRCamera.transform.TransformPoint(positionOffset);
        transform.rotation = VRPlatformSetting.instance.oVRCamera.transform.rotation * Quaternion.Euler(rotationOffset);
    }
    
}
