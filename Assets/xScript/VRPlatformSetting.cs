﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
//using Valve.VR;
using System.Threading.Tasks;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace DKP.VR
{
    public enum Hand
    {
        left, right
    }

    public static class ControlState
    {
        public static bool rayCasting = false;
        public static bool drawing = false;
        public static bool leftHandTouching = false;
        public static bool rightHandTouching = false;
    }

    public static class VRIO
    {
        async public static void Haptics(Hand hand)
        {
            if (hand == Hand.left)
            {
                OVRInput.SetControllerVibration(0.125f, 0.125f, OVRInput.Controller.LTouch);
                await Task.Delay(100);
                OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.LTouch);
            }
            else if (hand == Hand.right)
            {
                OVRInput.SetControllerVibration(0.125f, 0.125f, OVRInput.Controller.RTouch);
                await Task.Delay(100);
                OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.RTouch);
            }
            await Task.Yield();
        }
        public static void Throw(Hand hand, GameObject obj)
        {
            if (hand == Hand.left && obj.GetComponent<Rigidbody>() != null)
            {
                obj.GetComponent<Rigidbody>().velocity = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch);
                obj.GetComponent<Rigidbody>().angularVelocity = OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.LTouch);
            }
            else if (hand == Hand.right && obj.GetComponent<Rigidbody>() != null)
            {
                obj.GetComponent<Rigidbody>().velocity = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);
                obj.GetComponent<Rigidbody>().angularVelocity = OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.RTouch);
            }
        }
    }

    public class VRPlatformSetting : MonoBehaviour
    {
        public static VRPlatformSetting instance;

        [Header("Oculus Setting (camera)")]
        public GameObject oVRCameraRig;
        public Camera oVRCamera;
        public OVRControllerHelper handL;
        public OVRControllerHelper handR;

        private void Awake()
        {
            instance = this;
        }
    }
}