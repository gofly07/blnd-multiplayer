﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DKP.VR;
using Photon.Pun;
public class FollowObjectPosition : MonoBehaviour
{
    public bool isLeft = false;
    public Transform target;
    public Vector3 fixPos = Vector3.zero;
    [SerializeField] private PhotonView view;
    // Update is called once per frame
    void Update()
    {
        if (!view.IsMine)
            return;

        if (isLeft)
            target = VRPlatformSetting.instance.handL.transform;
        else if (!isLeft)
            target = VRPlatformSetting.instance.handR.transform;

        gameObject.transform.position = target.transform.TransformPoint(fixPos);
        gameObject.transform.rotation = target.transform.rotation;
    }
}
